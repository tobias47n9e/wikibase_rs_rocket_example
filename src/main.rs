// main.rs
//
// Copyright © 2018
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#![feature(custom_derive)]
#![feature(plugin)]

#![plugin(rocket_codegen)]

extern crate rocket;
extern crate rocket_contrib;
extern crate wikibase;

use rocket::Rocket;
use rocket::request::Form;
use rocket::response::NamedFile;
use rocket_contrib::Template;
use std::collections::HashMap;
use std::path::{Path, PathBuf};
use wikibase::{Configuration, Entity};

#[cfg(test)] mod tests;

const USER_AGENT: &'static str = "Wikibase-Rocket-Example/0.2.0";

#[derive(FromForm)]
struct Query {
    pub id: String,
}

#[get("/")]
fn index() -> Template {
    let context: HashMap<&str, &str> = HashMap::new();
    Template::render("index", &context)
}

fn render_entity_view_template_with_data(entity: Entity) -> Template {
    let mut context: HashMap<&str, &str> = HashMap::new();
    let label = entity.label_in_locale("en").unwrap_or_else(|| {""});
    let description = entity.description_in_locale("en").unwrap_or_else(|| {""});
    let entity_id = entity.id();
    context.insert("label", &label);
    context.insert("description", &description);
    context.insert("id", &entity_id);
    Template::render("detail", &context)
}

#[post("/", data="<query>")]
fn entity_view(query: Form<Query>) -> Template {
    let configuration = Configuration::new(USER_AGENT).unwrap();
    let entity = Entity::new_from_id(query.get().id.as_str(), &configuration);
    let context: HashMap<&str, &str> = HashMap::new();

    match entity {
        Ok(value) => {
            render_entity_view_template_with_data(value)
        }
        Err(_) => {
            Template::render("index", &context)
        }
    }
}

#[get("/<file..>")]
fn files(file: PathBuf) -> Option<NamedFile> {
    NamedFile::open(Path::new("static/").join(file)).ok()
}

fn rocket() -> Rocket {
    rocket::ignite()
    .attach(Template::fairing())
    .mount("/", routes![
        entity_view,
        files,
        index
    ])
}

fn main() {
    rocket().launch();
}
