use super::rocket;
use rocket::local::Client;
use rocket::http::{ContentType, Status};

#[test]
fn test_index() {
    let client = Client::new(rocket()).expect("valid rocket instance");
    let mut response = client.get("/").dispatch();
    assert_eq!(response.status(), Status::Ok);
    assert_eq!(response.body_string().unwrap().contains("(╯°□°）╯︵ ┻━┻"), true);
}

#[test]
fn test_detail() {
    let client = Client::new(rocket()).expect("valid rocket instance");
    let mut response = client.post("/")
        .body("id=Q472")
        .header(ContentType::Form)
        .dispatch();
    assert_eq!(response.status(), Status::Ok);
    assert_eq!(response.body_string().unwrap().contains("Sofia"), true);
}
