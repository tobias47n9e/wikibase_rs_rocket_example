# Wikibase RS + Rocket example

An example showing how to use Wikibase RS with Rocket.

## Installation

Clone the repository and then start the development server
from the project directory:

```bash
cargo run
```

## Contributing

This tool and the community around it are part of the Wikimedia movement.
Everybody is welcome to contribute. Plase refer to this guide
https://www.mediawiki.org/wiki/Code_of_Conduct
before contributing.

## Licence

GNU General Public License, Version 2 or later (GPL-2.0+)

## Repository / Issue tracker

https://gitlab.com/tobias47n9e/wikibase_rs_rocket_example

## Crates.io

https://crates.io/crates/wikibase_rs_rocket_example
